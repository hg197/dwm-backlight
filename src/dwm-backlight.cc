#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <cerrno>

#define STEPS 20

#define INTEL "/sys/class/backlight/intel_backlight"
#define ACPI  "/sys/class/backlight/acpi_video0"

int main(int argc, char ** argv)
{
	if(argc != 2)
	{
		printf("%s inc/dec\n",argv[0]) ;
		return 1 ;
	}

	if(strncmp(argv[1],"inc",3) != 0 and strncmp(argv[1],"dec",3) != 0)
	{
		printf("%s inc/dec\n",argv[0]) ;
		return 1 ;
	}

	char * cur  = new char[64] ;
	char * max  = new char[64] ;

	if( 0 == access(INTEL,F_OK) )
	{
		sprintf(cur,"%s/%s",INTEL,"brightness") ;
		sprintf(max,"%s/%s",INTEL,"max_brightness") ;
	}
	else if( 0 == access(ACPI,F_OK) )
	{
		sprintf(cur,"%s/%s",ACPI,"brightness") ;
		sprintf(max,"%s/%s",ACPI,"max_brightness") ;
	}
	else
	{
		printf("Error: neither %s nor %s\n",INTEL,ACPI) ;
		return 1 ;
	}

	int max_backlight	= 0 ;
	int cur_backlight	= 0 ;
	int step			= 0 ;

	FILE * F  = NULL ;

	// read max_backlight
	F = fopen(max,"r") ;
	fscanf(F,"%d",&max_backlight) ;
	fclose(F) ;

	// read cur_backlight
	F = fopen(cur,"r") ;
	fscanf(F,"%d",&cur_backlight) ;
	fclose(F) ;
	
	switch(max_backlight > 20)
	{
		case true:	step = max_backlight/STEPS ;
					break ;
		case false: step = 1 ;
					break ;
	}

	#ifdef DEBUG
		printf("MAX Brightness\t= %d\nCur Brightness\t= %d\nstep\t\t= %d\n",max_backlight,cur_backlight,step) ;
	#endif

	int brightness = 0 ;

	if(strncmp(argv[1],"dec",3) == 0)
	{
		if( cur_backlight - step > 0)
			brightness = cur_backlight - step ;
		else
			brightness = cur_backlight ;
	}
	if(strncmp(argv[1],"inc",3) == 0)
	{
		if( cur_backlight + step + 1 < max_backlight )
			brightness = cur_backlight + step ;
		else
			brightness = cur_backlight ;
	}

	// write brightness

	errno = 0 ;
	F = fopen(cur,"w") ;

	if(errno)
		printf("%s\n",strerror(errno)) ;
	else
	{
		fprintf(F,"%d",brightness) ;
		fclose(F) ;
	}

	delete [] cur ;
	delete [] max ;

return 0 ;
}
