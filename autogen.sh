#!/usr/bin/sh

aclocal --install -I m4 &&
autoheader &&
autoconf &&
automake --add-missing --copy &&
./configure "$@"

